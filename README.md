# Non-local goto (aka long jump)
  
The C language provides goto's which are limited to the scope within which they are declared (i.e. function scope).  The challenge in doing non-local goto's is in stack management.  This requires saving information such as stack pointer and program counter.  Additionally, it must be the case that the non-local goto's are valid only if the requisite activation frames are on the stack.

## Usage
  
  ```bash
  chmod a+x buildall
  ./buildall
  ./jmp
  ```
  
## Syntax
  
int **setjmp**(jmp_buf env);  
Returns 0 on initial call, nonzero on return triggered by call to longjump()
  

void **longjmp**(jmp_buf env, int val);  
Does not return functionally, the value of val is return value passed back to setjmp()
  
## Output
```
Commence jumping
Jumped into a: 0
Jumped into b: 0
Jumped into a: 1
Jumped into b: 1
Stop jumping
```