#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <jmp.h>

static jmp_buf just_jmp;
static jmp_buf jmp_a;
static jmp_buf jmp_b;

void jump_a(void)
{
    int val;

    val = setjmp(jmp_b);

    printf("Jumped into a: %d\n", val);

    if (!val)
    {
        jump_b();
    }

    longjmp(jmp_a, 1);
}

void jump_b(void)
{
    int val;

    val = setjmp(jmp_a);

    printf("Jumped into b: %d\n", val);

    if (!val)
    {
        longjmp(jmp_b, 1);
    }

    longjmp(just_jmp, 42);
}

int main(void)
{
    printf("Commence jumping\n");

    if (!setjmp(just_jmp))
    {
        jump_a();
    }

    printf("Stop jumping\n");

    return 0;
}
